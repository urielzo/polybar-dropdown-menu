# Archlabs i3-gaps

Dotfiles for polybar_dropdown & i3-gaps

## Preview

## Clean
![clean](/preview/clean.png)
<br />
## Dirty
![Dirty](/preview/Dirty.png)


## Details

* Distro: [ArchLabs](https://archlabslinux.com/)
* Window manager: [i3-gaps](https://aur.archlinux.org/i3-gaps-rounded-git.git)
* conky: [conky-lua-nv](https://aur.archlinux.org/conky-lua-nv.git)
* Launcher: [Rofi](https://github.com/davatorium/rofi)
* Panel: [Polybar](https://github.com/polybar/polybar.git)
* Compositor: [picom](https://github.com/jonaburg/picom)
* Terminal: [kitty & alacritty]
* Display: [1600x900]


## Fonts

* `NovaMono for Powerline`
* `DejaVu Sans`
* `Font Awesome 5 Free`
* `Font Awesome 5 Brands`
* `Iosevka Nerd Font`

### Relevant lines on ~/.config/i3/config

# Polybar is used instead of i3bar
* `exec_always --no-startup-id ~/.config/i3/scripts/polybar_wrapper launch`

# Open options menu
* `bindsym $mod+BackSpace exec --no-startup-id ~/.config/i3/scripts/polybar_wrapper options`

